#Libraries
import cv2
import sqlite3
import datetime
from picamera import PiCamera
from time import sleep
import RPi.GPIO as GPIO
import time, sys, serial
from xbee import XBee, ZigBee
camera=PiCamera()


detector = cv2.QRCodeDetector()
nom=""
prenom=""

database = sqlite3.connect('qrcodeBDD.db')
curseur = database.cursor()
curseur.execute("CREATE TABLE IF NOT EXISTS log (id TEXT NOT NULL, nom TEXT NOT NULL, prenom TEXT NOT NULL, date TEXT NOT NULL)")
curseur.execute("CREATE TABLE IF NOT EXISTS user (id TEXT NOT NULL UNIQUE)")
               

#curseur.execute("DROP TABLE user")
#curseur.execute("DROP TABLE log")
#database.close()
###################################ULTRASON######################################################################
 
#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)
 
#set GPIO Pins
GPIO_TRIGGER = 18
GPIO_ECHO = 24
 
#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)


def distance():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)
    time.sleep(0.01)
 
    # set Trigger after 0.01ms to LOW
    GPIO.output(GPIO_TRIGGER, False)
 
    StartTime = time.time()
    StopTime = time.time()
 
    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()
 
    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()
 
    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance = (TimeElapsed * 34300) / 2
 
    return distance

#############################################RELAIS ELECTRO AIMANT############################################################
from signal import signal, SIGINT
from sys import exit

def handler(signal_received, frame):
    # on gère un cleanup propre
    print('')
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    GPIO.cleanup()
    exit(0)

#############################################################################################################################
camread=True

#Opening of XBee' ports
ser=serial.Serial(port='/dev/ttyAMA0',baudrate=9600,parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE,bytesize=serial.EIGHTBITS,timeout=1)

if __name__ == '__main__':
    try:
        while True:
            #Data to be sent to the server
            #x=ser.readline()

            dist = distance()
            vertices_array=None
            data=None
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(4, GPIO.OUT)
            GPIO.output(4, GPIO.HIGH)
           
            if (dist<30 and dist>10):
                print("distance ok")
               
                time.sleep(0.6)
                camera.capture("image.png")
                filename="image.png"
                img=cv2.imread(filename)

                data, vertices_array, _ = detector.detectAndDecode(img)
                if vertices_array is not None:
                    print("Qr code detecté, analyse en cours")
                    print(data)
                    if data:
                        for i in range( len(data)):
                            if(data[i]=="$"):
                                nom=data[:i]
                                prenom=data[i+1:len(data)]

                        print("QR Code data: ", nom,prenom)

                        if (nom!="" and prenom!=""):
                            id=nom+prenom
                            checkid=curseur.execute("SELECT * FROM user WHERE id = (?)" ,(id,))
                            myresult = curseur.fetchall()
                            if (myresult==[]):
                                curseur.execute("INSERT INTO user VALUES (?);",(id,))
                                myresult = curseur.fetchall()
                            print(myresult)


                            idVerif=""
                            for x in myresult:
                                idVerif=x
                                print(x)

                            if (idVerif!=""):
                                GPIO.output(4, GPIO.LOW)
                                time.sleep(2)
                                now=datetime.datetime.now()
                                formatted_date=now.strftime('%Y-%m-%d %H:%M:%S')
                                curseur.execute("INSERT INTO log VALUES (?,?,?,?);", (id,nom,prenom,formatted_date))
                                nom = curseur.execute("SELECT nom FROM log")
                                prenom = curseur.execute("SELECT prenom FROM log")
                                database.commit()
                                mycursor=database.cursor()
                                mycursor.execute("SELECT * FROM log")
                                myresult = mycursor.fetchall()
                                print("Log:")
                                for x in myresult:
                                    print(x)
                                mycursor.execute("SELECT * FROM user")
                                myresult = mycursor.fetchall()
                                for x in myresult:
                                    print(x)
             
                               
 
    # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()
