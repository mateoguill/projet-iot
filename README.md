# FRICODE
***
### Description du projet
Ce projet a pour but de créer un verrou magnétique pour le frigo de la salle INEM. Il fait parti intégrante du projet FRINEM.

### Prérequis
Il faut l'application développée par l'autre équipe du projet FRINEM afin de générer un qrcode d'authenfication.
Lien vers l'application :https://qrco.de/bckwmQ  
/!\ MERCI DE NE PAS PARTAGER CE LIEN /!\

### Utilisation
Une fois le qrcode généré, il est possible d'ouvrir la porte magnétique. Pour se faire simplement, présenter le qrcode devant la caméra, s'il est reconnu dans la base de donnée la porte s'ouvrira.

### Fonctionnement
L'application génère un qrcode contenant le nom et prénom de l'utilisateur. Lorsqu'un mouvement est détécté proche de la caméra (grâce à un capteur ultrason), celle-ci s'allume et prends une photo. Si la photo contient un qrcode valide, le relai coupe le courant de l'électroaimant et ouvre la porte. Si la photo ne contient pas de qrcode ou qu'il n'est pas valide, la porte restera fermée. 
